package com.comercial.devicemanagement.service;

import java.util.List;

import com.comercial.devicemanagement.model.DeviceModel;

public interface DeviceService {

	List<DeviceModel> getAllDeviceList();

	void saveNewDevice(DeviceModel deviceModel);
}

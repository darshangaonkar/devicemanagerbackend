package com.comercial.devicemanagement.model;

public class DeviceModel {
	
	public DeviceModel() {
	
	}

	public DeviceModel(int deviceId, String deviceName, double deviceCost) {
		super();
		this.deviceId = deviceId;
		this.deviceName = deviceName;
		this.deviceCost = deviceCost;
	}

	private int deviceId;

	private String deviceName;

	private double deviceCost;

	public int getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(int deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public double getDeviceCost() {
		return deviceCost;
	}

	public void setDeviceCost(double deviceCost) {
		this.deviceCost = deviceCost;
	}

}

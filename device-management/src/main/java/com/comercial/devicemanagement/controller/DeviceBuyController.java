package com.comercial.devicemanagement.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.comercial.devicemanagement.model.DeviceModel;
import com.comercial.devicemanagement.service.DeviceService;

@CrossOrigin("*")
@RestController
@RequestMapping("/buy")
public class DeviceBuyController {
	
	@Autowired
	DeviceService deviceService;

	@PostMapping("/addboughtDevice")
	public void addDevice(@RequestBody DeviceModel deviceModel) {
          deviceService.saveNewDevice(deviceModel);
	}
}

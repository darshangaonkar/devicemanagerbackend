package com.comercial.devicemanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.comercial.devicemanagement.model.DeviceModel;
import com.comercial.devicemanagement.service.DeviceService;

@CrossOrigin("*")
@RestController
@RequestMapping("/sales")
public class DeviceSalesController {
	
	@Autowired
	DeviceService deviceService;

	@GetMapping("/getAllPurchasedDeviceList")
	public List<DeviceModel> getAllDeviceList() {
		 List<DeviceModel> allPurchasedDeviceList = deviceService.getAllDeviceList();		
		return allPurchasedDeviceList;
	}
}

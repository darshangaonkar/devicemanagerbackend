package com.comercial.devicemanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.comercial.devicemanagement.entity.Device;

@Repository
public interface DeviceRepository extends JpaRepository<Device, String>{

	List<Device> findAll();
	
}

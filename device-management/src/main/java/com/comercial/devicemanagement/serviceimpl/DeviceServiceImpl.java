package com.comercial.devicemanagement.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.comercial.devicemanagement.entity.Device;
import com.comercial.devicemanagement.model.DeviceModel;
import com.comercial.devicemanagement.repository.DeviceRepository;
import com.comercial.devicemanagement.service.DeviceService;

@Service
public class DeviceServiceImpl implements DeviceService {

	@Autowired
	DeviceRepository deviceRepository;

	@Override
	public List<DeviceModel> getAllDeviceList() {

		List<Device> allDeviceList = deviceRepository.findAll();

		List<DeviceModel> deviceModelsList = new ArrayList<>();

		DeviceModel deviceModel = new DeviceModel();

		for (int i = 0; i < allDeviceList.size(); i++) {
			deviceModel.setDeviceId(allDeviceList.get(i).getDeviceId());
			deviceModel.setDeviceCost(allDeviceList.get(i).getDeviceCost());
			deviceModel.setDeviceName(allDeviceList.get(i).getDeviceName());

			deviceModelsList.add(deviceModel);
		}

		return deviceModelsList;
	}

	@Override
	public void saveNewDevice(DeviceModel deviceModel) {
		Device device = new Device();
		device.setDeviceId(deviceModel.getDeviceId());
		device.setDeviceCost(deviceModel.getDeviceCost());
		device.setDeviceName(deviceModel.getDeviceName());
		deviceRepository.save(device);
	}

}

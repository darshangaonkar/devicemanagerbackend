package com.comercial.devicemanagement.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Device {
	
	public Device() {
	
	}

	public Device(int deviceId, String deviceName, double deviceCost) {
		super();
		this.deviceId = deviceId;
		this.deviceName = deviceName;
		this.deviceCost = deviceCost;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int deviceId;

	@Column(name = "deviceName")
	private String deviceName;

	@Column(name = "deviceCost")
	private double deviceCost;

	public int getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(int deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public double getDeviceCost() {
		return deviceCost;
	}

	public void setDeviceCost(double deviceCost) {
		this.deviceCost = deviceCost;
	}

}
